import React from "react";
import "./index.css";
import Square from "./square";

export default class Board extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstPlayer: true,
      status: "Please start the game",
      player1Game: [],
      player2Game: [],
      gameEnable: true
    };
  }

  addSquare = position => {
    return (
      <Square
        disabled={!this.state.gameEnable}
        saveTurn={this.saveTurn}
        player={this.state.firstPlayer}
        position={position}
      />
    );
  };

  saveTurn = (player, position) => {
    this.setState({ status: "Game in progress" });
    const player1Game = this.state.player1Game;
    const player2Game = this.state.player2Game;
    let finish = false;
    if (this.state.firstPlayer) {
      player1Game.push(position);
      this.setState({ player1Game: player1Game });
      if (this.checkWinCondition(player1Game)) {
        this.setState({ status: "Player 1 win", gameEnable: false });
        finish = true;
      }
    } else {
      player2Game.push(position);
      this.setState({ player2Game: player2Game });
      if (this.checkWinCondition(player2Game)) {
        this.setState({ status: "Player 2 win", gameEnable: false });
        finish = true;
      }
    }
    if (
      this.state.player1Game.length + this.state.player2Game.length === 9 &&
      !finish
    ) {
      this.setState({ status: "Nobody won", gameEnable: false });
    }
    this.changePlayer(player);
  };

  changePlayer(player) {
    this.setState({ firstPlayer: !player });
  }

  checkWinCondition(playerGame) {
    if (
      (playerGame.includes(1) &&
        playerGame.includes(2) &&
        playerGame.includes(3)) ||
      (playerGame.includes(4) &&
        playerGame.includes(5) &&
        playerGame.includes(6)) ||
      (playerGame.includes(7) &&
        playerGame.includes(8) &&
        playerGame.includes(9)) ||
      (playerGame.includes(1) &&
        playerGame.includes(4) &&
        playerGame.includes(7)) ||
      (playerGame.includes(2) &&
        playerGame.includes(5) &&
        playerGame.includes(8)) ||
      (playerGame.includes(3) &&
        playerGame.includes(6) &&
        playerGame.includes(9)) ||
      (playerGame.includes(1) &&
        playerGame.includes(5) &&
        playerGame.includes(9)) ||
      (playerGame.includes(3) &&
        playerGame.includes(5) &&
        playerGame.includes(7))
    ) {
      return true;
    } else {
      return false;
    }
  }

  reset = () => {
    window.location.reload();
  };

  render() {
    let playerTurn = this.state.firstPlayer
      ? "Player 1's turn"
      : "Player 2's turn";
    return (
      <div>
        <div
          hidden={
            this.state.status !== "Game in progress" &&
            this.state.status !== "Please start the game"
          }
          className="playerTurn"
        >
          {playerTurn}
        </div>
        <div className={"status " + (!this.state.gameEnable ? "red" : {})}>
          {this.state.status}
        </div>
        <div className="board-row">
          {this.addSquare(1)}
          {this.addSquare(2)}
          {this.addSquare(3)}
        </div>
        <div className="board-row">
          {this.addSquare(4)}
          {this.addSquare(5)}
          {this.addSquare(6)}
        </div>
        <div className="board-row">
          {this.addSquare(7)}
          {this.addSquare(8)}
          {this.addSquare(9)}
        </div>
        <button onClick={this.reset} hidden={this.state.gameEnable}>
          Reset
        </button>
      </div>
    );
  }
}
