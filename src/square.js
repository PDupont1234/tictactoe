import React from "react";
import "./index.css";

export default class Square extends React.Component {
  constructor({ player, position }) {
    super();
    this.state = { squareStatus: "", position: position };
  }

  fullSquare = () => {
    document.getElementById(this.state.position).style.color = this.props.player
      ? "blue"
      : "red";
    if (this.props.player) {
      this.setState({ squareStatus: DRAWING.CROSS });
      this.saveTurn(this.props.player, this.props.position);
    } else {
      this.setState({ squareStatus: DRAWING.ROUND });
      this.saveTurn(this.props.player, this.props.position);
    }
  };

  saveTurn = (player, position) => {
    this.props.saveTurn(player, position);
  };
  render(props) {
    return (
      <button
        disabled={this.state.squareStatus !== "" || this.props.disabled}
        onClick={this.fullSquare}
        className="square"
      >
        <span id={this.state.position}>{this.state.squareStatus}</span>
      </button>
    );
  }
}

export const DRAWING = {
  CROSS: "X",
  ROUND: "O"
};
