import React from "react";
import Game from "../game";
import Board from "../board";
import Square from "../square";
import { shallow } from "enzyme";

describe("Game", () => {
  it("should have a Board", () => {
    const game = shallow(<Game />);
    expect(game.find(Board).length).toBe(1);
  });
});

describe("Board", () => {
  it("should have 9 square", () => {
    const board = shallow(<Board />);
    expect(board.find(Square).length).toBe(9);
  });
});

describe("Square", () => {
  it("should have a button", () => {
    const square = shallow(<Square />);
    expect(square.find("button").length).toBe(1);
  });
});

describe("Button", () => {
  it("should have empty value", () => {
    const button = shallow(<button />);
    expect(button.find("button").text()).toEqual("");
  });
});
