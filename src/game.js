import React from "react";
import "./index.css";
import Board from "./board";

export default class Game extends React.Component {
  render() {
    return (
      <div className="game">
        <div className="game-board">
          <h1 style={{ color: "red", fontFamily: "fantasy" }}>Tic Tac Toe</h1>
          <Board />
        </div>
      </div>
    );
  }
}
